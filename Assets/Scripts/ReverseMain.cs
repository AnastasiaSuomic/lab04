﻿using System;
using System.Linq;

public class ReverseMain : Main
{
        public string GetResult(string name, string surname)
        {
            var reversedName = new string(name.Reverse().ToArray());
            var reversedSurname = new string(surname.Reverse().ToArray());
            var result = $"{reversedName} {reversedSurname}";
            return result;
        }

        protected override void DoAction()
        {
            var result = GetResult(inputName.text, inputSurname.text);
            SetTextResult(result);
            AddToHistory();
            ShowToast(result);
        }

        public override void AddToHistory()
        {
            var name = inputName.text;
            var surname = inputSurname.text;
            var result = GetResult(name, surname);
            var message = $"Reversed {name} {surname} to {result}";
            HistoryManager.Instance.AddMessageToHistory(message, result);
        }
}