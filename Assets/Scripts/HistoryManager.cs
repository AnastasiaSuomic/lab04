﻿using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

public class HistoryManager
{
    public List<HistoryItem> Items;

    private static HistoryManager _instance;
    //реализация паттерна синглтон
    public static HistoryManager Instance{get{if (_instance == null) _instance = new HistoryManager();
        return _instance;
    }}

    private HistoryManager()
    {
        _instance = this;
        LoadItems();
    }

    public void LoadItems()
    {
        var json = PlayerPrefs.GetString("history");
        if (!string.IsNullOrEmpty(json))
            Items = JsonConvert.DeserializeObject<List<HistoryItem>>(json);
        
        if (Items == null)
            Items = new List<HistoryItem>();
    }

    public void SaveItems()
    {
        var json = JsonConvert.SerializeObject(Items);
        PlayerPrefs.SetString("history", json);
        PlayerPrefs.Save();
    }

    public void Clear()
    {
        Items?.Clear();
        SaveItems();
    }
    
    public void AddMessageToHistory(string message, string result)
    {
        var item = new HistoryItem(message, result);
        Items.Add(item);
        SaveItems();
    }
}