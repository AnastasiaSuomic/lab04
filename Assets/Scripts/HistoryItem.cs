﻿using System;

[Serializable]
public class HistoryItem
{
    public string Message;
    public string Result;

    public HistoryItem(string message, string result)
    {
        Message = message;
        Result = result;
    }
}