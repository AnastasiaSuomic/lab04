﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityNative.Toasts.Example;

public class HistoryItemVisual : MonoBehaviour
{
    [SerializeField] private Button buttonToastHistory;
    [SerializeField] private Text textHistory;
    private string _result;
    
    protected void ShowToast(string message)
    {
#if UNITY_ANDROID
        UnityNativeToastsHelper.ShowShortText(message);
#endif
    }

    private void Awake()
    {
        buttonToastHistory.onClick.AddListener(ButtonToastHistoryOnClick);
    }

    private void ButtonToastHistoryOnClick()
    {
        ShowToast(_result);
    }

    public void LoadItem(HistoryItem historyItem)
    {
        textHistory.text = historyItem.Message;
        _result = historyItem.Result;
    }
}