﻿using System;
using UnityEngine;

public class LocalizationController : MonoBehaviour
{
    [SerializeField] private LocalizationList list;
    private Language _currentLanguage;
    //реализация паттерна синглтон
    public static LocalizationController instance;
    public event Action<Language> languageChanged; 
    
    private void Awake()
    {
        instance = this;
        switch (Application.systemLanguage)
        {
            case SystemLanguage.Russian: 
                _currentLanguage = Language.ru;
                break;
            default: 
                _currentLanguage = Language.en;
                break;
        }
    }

    public string GetLabel(string tag)
    {
        return GetLabel(tag, _currentLanguage);
    }
    
    public string GetLabel(string tag, Language language)
    {
        foreach (var item in list.items)
        {
            if (item.tag == tag)
            {
                switch (language)
                {
                    case Language.en: return item.en;
                    case Language.ru: return item.ru;
                }
            }
        }

        return tag;
    }

    private void OnLanguageChanged(Language newLanguage)
    {
        languageChanged?.Invoke(newLanguage);
    }

    public void SwitchLanguage(Language language)
    {
        _currentLanguage = language;
        OnLanguageChanged(_currentLanguage);
    }
}