﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HistoryViewer : MonoBehaviour
{
    [SerializeField] private HistoryItemVisual itemPrefab;
    [SerializeField] private Transform itemsContainer;
    [SerializeField] private Button buttonCloseHistory;
    [SerializeField] private Button buttonClearHistory;
    private List<HistoryItemVisual> _spawnedItems;

    private void Awake()
    {
        _spawnedItems = new List<HistoryItemVisual>();
        buttonCloseHistory.onClick.AddListener(ButtonCloseHistoryOnClick);
        buttonClearHistory.onClick.AddListener(ButtonClearHistoryOnClick);
    }

    private void ButtonCloseHistoryOnClick()
    {
        gameObject.SetActive(false);
    }
    
    private void ButtonClearHistoryOnClick()
    {
        HistoryManager.Instance.Clear();
        DestroyItems();
    }

    private void DestroyItems()
    {
        foreach (var item in _spawnedItems)
        {
            Destroy(item.gameObject);
        }
        _spawnedItems.Clear();
    }

    private void SpawnItems()
    {
        DestroyItems();
        foreach (var item in HistoryManager.Instance.Items)
        {
            var panel = Instantiate(itemPrefab, itemsContainer);
            panel.LoadItem(item);
            _spawnedItems.Add(panel);
        }
    }

    private void OnEnable()
    {
        SpawnItems();
    }
}